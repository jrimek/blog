# Hugo based blog with panr/hugo-theme-terminal theme

#### create a new blog post:

```
hugo new posts/POST_NAME
```

#### start server for local development

```
hugo server -D
```

#### deloyment: 

```
hugo 
cd aws
tsc
cdk diff
cdk deploy
```
