---
title: "About"
---

My name is Jonny Rimek. I've been a software engineer for 3 years. 

Currently I work at [Healy GmbH](https://www.healyworld.net/in/wellness/) and help grow and optimise their distribution platform. In my free time I build my own bootstraped business [wowmate.io](https://wowmate.io). It's a SaaS that helps World of Warcraft players analyze their log files in order to improve.

I care alot privacy and security in general. In order to improve my privacy I use Ubuntu as operating system, Signal as a messanger, ProtonMail for E-Mail, ProtonVPN as VPN, Firefox as a Webbrowser and DuckDuckGo as my primary searchengine. To secure my logins I use U2F with using the [Nitrokey](https://shop.nitrokey.com/shop/product/nitrokey-fido-u2f-20) where possible.


#### other interests in random order:

- golang
- aws/severless
- self improvement (from tech skills (vim<3)) to real life (working out))
- mitigating climate change
- human psychology
- the brain
- dogs
- iceland

#### recruitment

I'm not actively looking for a job right now, if you want to reach out anyway please check out some of my [expectations](https://gitlab.com/jrimek/job-requirements/) for a new job. My E-Mail is j.rimek@protonmail.com


